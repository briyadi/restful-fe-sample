const BASE_URL = "http://localhost:3000"
const form = document.getElementById("form")
const title = document.getElementById("title")
const body = document.getElementById("body")

const query = location.search || ""
const id = query.replace('?id=', '')
form.onsubmit = function(e) {
    // menahan supaya tidak redirect
    e.preventDefault()

    // lakukan pengiriman data ke server
    saveData()
}

function loadData() {
    if (id) {
        fetch(`${BASE_URL}/api/v1/posts/${id}`)
        .then(res => res.json())
        .then(post => {
            title.value = post.title || ""
            body.value = post.body || ""
        })
    }
}

function saveData() {
    let payload = {
        title: title.value,
        body: body.value
    }
    // jika id tidak ada berarti insert
    if (!id) {
        fetch(`${BASE_URL}/api/v1/posts`, {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify(payload)
        })
        .then(res => {
            location.href = "table.html"
        })
    }
    // jika id tersedia berarti update
    else {
        fetch(`${BASE_URL}/api/v1/posts/${id}`, {
            method: "PUT",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify(payload)
        })
        .then(res => {
            location.href = "table.html"
        })
    }
}

loadData()