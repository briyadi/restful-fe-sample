const BASE_URL = "http://localhost:3000"
const tableBody = document.getElementById('data')

function loadData() {
    fetch(`${BASE_URL}/api/v1/posts`)
    .then(res => res.json())
    .then(posts => {
        tableBody.innerHTML = ""
        for (let i in posts) {
            let post = posts[i]
            tableBody.innerHTML += `
            <tr>
                <td>${post.id}</td>
                <td>${post.title}</td>
                <td>
                    <a href="form.html?id=${post.id}">Edit</a>
                    <a href="" class="btn-remove" data-id="${post.id}">Remove</a>
                </td>
            </tr>`
        }
    })
}

tableBody.addEventListener('click', e => {
    if (e.target.classList.contains("btn-remove")) {
        e.preventDefault()
        let id = e.target.getAttribute("data-id")
        fetch(`${BASE_URL}/api/v1/posts/${id}`, {
            method: "DELETE"
        }).then(res => {
            loadData()
        })
    }
})

loadData()